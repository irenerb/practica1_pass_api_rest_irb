// definimos puerto de escucha
var express = require('express');
var app = express();
var port = process.env.PORT || 3000;

// definimos bodyParser para poder interpretar el body
var bodyParser = require('body-parser');
app.use(bodyParser.json());

//variables para trabajar con MLAB
var baseMlabURL= "https://api.mlab.com/api/1/databases/apitechu_irb/collections/";
var mLabAPIKey ="apiKey=w6OneIggl-HKBf_JyeOcIT6CzhRrtbiS";
var requestJson =require("request-json");

/* --------------------------------- */

app.listen(port);
console.log("API escuchando en el puerto: "+ port);

/* Creamos método GET: va a pasar una función como parámetro
como trabajamos con un framework, éste nos va a rellenar los parámetros de
la función req y res */
app.get('/apitechu/v1',
  function(req, res){
      console.log("GET /apitechu/v1");

      //res.send("HOla desde API Tech U");
      /* respuesta en formato json: */
      res.send(
        {
          "msg": "Bienvenido a la API de Tech U - IreneR"
        }
      );
  }
);

app.get('/apitechu/v1/users',
  function(req, res) {
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json', {root: __dirname});
    //Deprecada --> // res.sendfile('./usuarios.json');
  }
);

// versionamos api 'users' --> v2
app.get('/apitechu/v2/users',
  function(req, res) {
    console.log("GET /apitechu/v2/users");

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente para peticiones http creado");

    // lanzamos un GET concatenando nuestra APIKEY
    httpClient.get("user?" + mLabAPIKey,
      //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
      function(err, resMLab, body) {
        var response = !err ? body : {
          "msg" : "Error lanzando GET /v2/users"
        }
        //devolvemos respuesta de 'express'
        res.send(response);
      }
    );
  }
);

app.get('/apitechu/v1/users',
  function(req, res) {
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json', {root: __dirname});
    //Deprecada --> // res.sendfile('./usuarios.json');
  }
);

// v2 - búsqueda por id
app.get('/apitechu/v2/users/:id',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id");
    var id= req.params.id;
    var query= 'q={"id": ' + id + '}' ;

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente para peticiones http creado");

    // lanzamos un GET concatenando nuestra APIKEY
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario con id ="+ id
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario con id="+ id+" no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    );
  }
);


app.post('/apitechu/v1/users',
  function(req, res) {
    console.log("POST /apitechu/v1/users");
  //  console.log(req);
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };
    console.log("last_name = " + req.body.last_name);
    console.log("first_name = " + req.body.first_name);
    console.log("country = " + req.body.country);

    var users = require('./usuarios.json');
    users.push(newUser);
    console.log("Usuario '"+ req.body.first_name + "' en array");
    /* //añadir en la salida que vemos al invocar url
    res.send(users);

    //mostrar msg en la salida que vemos al invocar url
    res.send(
      {
        "MSG" : "Usuario añadido con éxito"
      }
    );
    */

    writeUserDataToFile(users);
  }
);

/*******************************************************************************
/* PRÁCTICA 1: LOGIN
/* 2 parámetros por body = email + password
/* Busca email igual al enviado:
/*    Si no está el email, login incorrecto.
/*    Si está el email
/*      y password coincide: login correcto + añadir propiedad "logged" = "true"
/*      si no es igual el password: login incorrecto.
*******************************************************************************/
app.post('/apitechu/v1/login',
  function(req, res) {
    console.log("POST /apitechu/v1/login");
    var userLogged = {
      "email" : req.body.email,
      "password" : req.body.password
    };
    console.log("LOGADO = " + req.body.email);

    var users = require('./usuarios.json');

    for (user of users) {
      if(user.email == userLogged.email)  {
        console.log("Encontrado: "+ userLogged.email);

        if(user.password == userLogged.password) {
          user.logged = true;
          writeUserDataToFile(users);
          res.send(
            {
              "MSG" : "Login correcto",
              "idUsuario": user.id
            }
          );
         }
         else{
            res.send(
              {
                "MSG" : "Login incorrecto: password no coincide"
              }
            );
         }

         break;
        } else {
          console.log("No: "+ user.email);
        }
    }
    // El mail no existe en el fichero de usuarios
    res.send(
      {
        "MSG" : "Login incorrecto: usuario no encontrado"
      }
    );
  }
);

/************************************************************
/* PRÁCTICA 1: LOGOUT
/* parámetro por body = id
/* Elimina la propiedad 'logged' y saca mensaje informativo:
/*     logout correcto / incorrecto
*************************************************************/
app.post('/apitechu/v1/logout',
  function(req, res) {
    console.log("POST /apitechu/v1/logout");
    var userLogged = {
      "id" : req.body.id
    };
    console.log("DES-Logar a = " + req.body.id);

    var users = require('./usuarios.json');
    for (user of users) {
      if(user.id == userLogged.id)  {
        console.log("Encontrado: "+ userLogged.id);
        if(user.logged == true) {
          delete user.logged;
          writeUserDataToFile(users);
          res.send(
            {
              "MSG" : "Logout correcto",
              "idUsuario": user.id
            }
          );
        }
        else{
          console.log("Logout: usuario no estaba logado: "+ userLogged.id);
          res.send(
            {
              "MSG" : "Logout: usuario no estaba logado"
            }
          );
        }

        break;
      } else {
        console.log("No: "+ user.id);
      }
    }
    res.send(
      {
        "MSG" : "Logout incorrecto: usuario no encontrado"
      }
    );
  }
);

/* FIN código práctica 1 ---------------------------------------------- */

// Eliminar usuario del fichero (E=id)
app.delete('/apitechu/v1/users/:id',
  function(req, res){
      console.log("DELETE /apitechu/v1/users/:id");

      var users = require('./usuarios.json');
      users.splice(req.params.id -1, 1);
      writeUserDataToFile(users);

      res.send(
        { "MSG" : "Usuario posición" + (req.params.id -1) + " borrado con éxito"}
      );
  }
)
// Función para escribir en el fichero usuarios.json
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(
    "./usuarios.json",
    jsonUserData,
    "utf8",
    function(err) {
      if(err) {
        console.log(err);
      } else {
        console.log("OK. WRITE fichero");
      }
    }
  )
}

// EJemplo paso de parámetros por url, por query, por headers y por body
app.post('/apitechu/v1/parametros/:p1/:p2',
  function(req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);

//-----------------------------------------------
// LOGIN V2 - versión profesor
//-----------------------------------------------
app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");
   var email = req.body.email;
   var password = req.body.password;

   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }
         res.send(response);
       } else {
         console.log("Got a user with that email and password, logging in");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario logado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

//-----------------------------------------------
// LOGOUT V2 - versión profesor
//-----------------------------------------------
app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)
//---------------------------------------------------
/* LOGIN v2
* get a mongo con usuario y password y que me devuelva el objeto entero
* post a mongo para marcar usuario logado
*/
/*
app.post('/apitechu/v2/login/',
  function(req, res) {
    console.log("post /apitechu/v2/login/");
//NO DEFINIR ASÍ LA VARIABLE: No es necesario!
// mejor dorectamente, sin variable... = req.body.email y req.body.password
    var userLogged = {
      "email" : req.body.email,
      "password" : req.body.password
    };
    console.log("LOGADO = " + req.body.email);

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente para peticiones http creado");

    //busco usuario en mongo
    var query= 'q={"email": "' + userLogged.email + '" , "password": "' + userLogged.password + '"}' ;

    // GET para recuperar usuario (si existe y password ok)
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario con email ="+ req.body.email
           }
           res.status(500);
           res.send(response);
         } else {
           if (body.length > 0) {
             //encuentro usuario y pwd
             response = body[0];
             console.log("Email encontrado con id=" +body[0].id );

             var queryPut= 'q={"id": ' + body[0].id + '}' ;
             var bodyPut= '{"$set" :{"logged":true}}';

             // put para marcar usuario logado
             httpClient.put("user?" + queryPut + "&" + mLabAPIKey, JSON.parse(bodyPut),
               //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
               function(errPut, resMLabPut, bodyPutFunc) {
                  if (errPut) {
                    console.log(errPut);
                    response = {
                      "msg" : "Error marcando login de usuario con id=" +body[0].id
                    }
                    res.status(500);
                  } else {
                    console.log("Usuario con id=" +body[0].id +" logado");
                  }
                }
             );
           } else {
             console.log("Usuario con email="+ req.body.email+" no encontrado o pwd incorrecta.");
             response = {
               "msg" : "Usuario con email="+ req.body.email+" no encontrado o pwd incorrecta."
             };
             res.status(404);
           }
        }
        res.send(response);
      }
    );
  }
);
*/

/* LOGOUT v2
* get a mongo con usuario y password y que me devuelva el objeto entero
* post a mongo para eliminar usuario logado
*/
/*
app.post('/apitechu/v2/logout/',
  function(req, res) {
    console.log("post /apitechu/v2/logout/");
    console.log("LOGADO = " + req.body.email);

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente para peticiones http creado");

    //busco email en mongo
    var query= 'q={"email": "' +req.body.email + '", "logged":' + true + '}' ;

    // GET para recuperar usuario (si existe y password ok)
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario con email ="+ req.body.email
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             //encuentro usuario y pwd
             response = body[0];
             console.log("Email encontrado con id=" +body[0].id );

             var queryPut= 'q={"id": ' + body[0].id + '}' ;
             var bodyPut= '{"$unset" :{"logged":"" }}';

             // put para marcar usuario logado
             httpClient.put("user?" + queryPut + "&" + mLabAPIKey, JSON.parse(bodyPut),
               //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
               function(errPut, resMLabPut, bodyPutFunc) {
                  if (errPut) {
                    console.log(errPut);
                    response = {
                      "msg" : "Error eliminando login del usuario con id=" +body[0].id
                    }
                    res.status(500);
                  } else {
                    console.log("Fin login del usuario con id=" +body[0].id );
                  }
                }
             );
           } else {
             console.log("Usuario con email="+ req.body.email+" no encontrado o no logado.");
             response = {
               "msg" : "Usuario con email="+ req.body.email+" no encontrado o no logado."
             };
             res.status(404);
           }
        }
        res.send(response);
      }
    );
  }
);
*/
