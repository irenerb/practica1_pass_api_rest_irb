# esto es un comentario
# Imagen raíz
FROM node

#carpeta raíz donde voy a trabajar en los contenedores
WORKDIR /apitechu_irb

# copiar los archivos de mi carpeta raíz (esquema_proyecto) en el WORKDIR
ADD . /apitechu_irb

# creamos un volumen para los logs
VOLUME ['\logs']

# Exponer el puerto en el que funciona mi api
EXPOSE 3000

# Instalar dependencias - RUN: ejecuta, y sigue cuando acabe
RUN npm install

# Comando para iniciar nuestra API -CMD: lanza y no espera
CMD ["npm", "start"]

#

#
